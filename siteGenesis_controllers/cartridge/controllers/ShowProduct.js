'use strict';

/**
 * Controller that renders the Hello page.
 *
 * @module controllers/Hello
 */

// API includes
var ProductMgr = require('dw/catalog/ProductMgr');
var ISML = require('dw/template/ISML');
var Pipelet = require('dw/system/Pipelet');
// Script modules
var guard = require('~/cartridge/scripts/guard');

/**
 * Render template WT2.4/product with property product
 * 
 * @returns PIPLET_NEXT | PIPLET_ERROR
 * 
 */
function start() {
	var params = request.httpParameterMap;
	var productID = params.product.stringValue;
	var product = ProductMgr.getProduct(productID);
	
	if (product) {
		ISML.renderTemplate('WT2.4/product', {
			Product: product
		});
		
		return true;
	}else {
		ISML.renderTemplate('WT2.4/productnotfound', {
			Log: "The product was not found id " + productID
		});
		
		return false;
	}
}

/*
 * Export the publicly available controller methods
 */
exports.Start = guard.ensure(['get'], start);
/** @see module:controllers/Home~includeHeader */
